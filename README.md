# Pandemics recipe: default DOCX

This repo contains the default Pandemics recipe for DOCX format.

As this the default format for DOCX format, you just need to specify that you want to use the DOCX format to apply this recipe:

```
---
pandemics:
  format: docx
---
```

If you want to explicitly reference this recipe, eg. to use an older version, just add in the YAML front-matter of your markdown document:

```
---
pandemics:
  recipe: gitlab.com:pandemics/recipe-default-docx SHA

---
```

where `SHA` is the commit number of the version you want to use.
